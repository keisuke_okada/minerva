chrome.webRequest.onBeforeSendHeaders.addListener(
    function (details) {
        details.requestHeaders = details.requestHeaders.map(
            function (header) {
                if (header.name === 'User-Agent') {
                    header.value = 'Mozilla/5.0 (Linux; U; Android 4.2.2; ja-jp; SC-04E Build/JDQ39) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30 GGEE/13.11.00 (and-web; 300)';
                }
                
                return header;
            }
        );

        return {requestHeaders: details.requestHeaders};
    },
    {
        urls : ["*://*.g-gee.info/*"]
    },
    [
        "blocking", "requestHeaders"
    ]);

chrome.runtime.onMessage.addListener(
    function (message, sender, sendResponse) {
        if (message.gacha) {
            chrome.tabs.query(
                {title:'ガチャ:*'},
                function (tabs){
                    tabs.forEach(
                        function (tab) {
                            chrome.tabs.sendMessage(
                                tab.id,
                                message
                            );
                        }
                    );
                });
        }
    }
);
