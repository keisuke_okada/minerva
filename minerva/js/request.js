chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        if (request.id == 'RaQs2-disable') {
            var alertmessage = 'このページは無効になりました。';
            var container = document.getElementById('container');
            if (container.innerHTML != alertmessage) {
                container.innerHTML = alertmessage;
                sendResponse(true);
            }
            else {
                sendResponse(false);
            }
        }
    }
);