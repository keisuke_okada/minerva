var data = JSON.parse(localStorage.getItem('data'));

var blacklist = JSON.parse(localStorage.getItem('blacklist')||'{}');

var params = getparams(location.href);
// クッキー
var cookies = document.cookie.split(';')
    .reduce(
        function (hash, x) {
            var datas = x.split('=');
            if (hash[datas[0]]) {
                if (!(hash[datas[0]] instanceof Array)) {
                    hash[datas[0]] = [hash[datas[0]]];
                }
                hash[datas[0]].push(datas[1]);
            }
            else {
                hash[datas[0]] = datas[1];
            }
            return hash;
        },{});

//<div class="button small"><a href="http://minerva.g-gee.info/index.php?view=friendDelete&amp;friendid=190019928407&amp;_t=1&amp;opensocial_owner_id=100019732565&amp;h=1393071537&amp;guid=ON" class="darkred">仲間から外す</a></div>

var view = {
    event_pvpTop : function () {
        document.title = 'イベ:' + document.title;
        var action = {
            ranking : addevebattle,
            top : evetop
        };
        if (action[params.action]) {
            action[params.action]();
        }
    },
    event_pvpBattleResult : function () {
        document.title = 'イベ結果:' + document.title;
        $('.navi').append(
            '<div class="h_menu"><a href="'
            + makeurl(
                'http://minerva.g-gee.info/index.php',
                {
                    view : 'event_pvpTop',
                    action : 'ranking',
                    eventid : 1,
                    _t : 1,
                    opensocial_owner_id : 100019732565,
                    h : Math.round(new Date().getTime() / 1000),
                    guid : 'ON'
                })
            + '" style="width:314px;border-radius:5px;">総合ランキング</a></div>');
    },
    questList : questlist,
    encounterBoss : function () {
        document.title = 'ボス:' + document.title;
        // 強襲戦
        encounterBoss();
//        http://minerva.g-gee.info/index.php?view=flashEncounterBossBattle&questid=&encounterid=27099166&deckid=5&_t=1&opensocial_owner_id=100019732565&h=1393059467&guid=ON
    },
    encounterBossWeek : function () {
        document.title = 'ボス履歴:' + document.title;
        $('a > table')
            .parent()
            .each(
                function () {
                    var params = getparams($(this).attr('href'));
                    $(this).after('<input type="button" questid="'
                                   + params.storyid * 4
                                   + '" value="自動実行" class="red autoquest">');
                }
            );
        $('.autoquest').click(
            function () {
                $(this).val('実行中').after("行動:<span id='act'></span>");
                bossquest($(this).attr('questid'), 0);
            });
    },
    gacha : function () {
        document.title = 'ガチャ:' + document.title;
        if ($('a.gacha').length > 0) {
            chrome.runtime.onMessage.addListener(samegacha);
            $('#head_menu')
                .after('<div class="button">'
                       + '<a class="red" id="samegacha">同時押し</a>'
                       + '</div>');
    
            $('#samegacha').click(
                function () {
                    chrome.runtime.sendMessage(
                        {
                            gacha : params.type
                        }
                    );
                }
            );
        }
        var types = {
            normal2Gacha : function () {
                if ($('form').length == 0) {
                    $('.navi')
                        .before(
                            '<form action="'
                                + makeurl(
                                    'http://minerva.g-gee.info/index.php',
                                    {
                                        view : 'gacha',
                                        action : 'gachaByFriendPoint',
                                        uniqId : 'NG2',
                                        _t : 1,
                                        opensocial_owner_id : 100019732565,
                                        h : Math.round(new Date().getTime() / 1000),
                                        guid : 'ON'
                                    })
                                + '" method="post" class="gacha"><div class="button"><input type="submit" value="ガチャる" class="red"></div></form>');
                }
            }
        },
        call = types[params.type]
        ;
        if (call) {
            call();
        }
        
    },
    event_questQuest : function () {
        document.title = 'イベ:幻想のミネルバナイツ';

        $('#head_menu')
            .after('<div class="button">'
                   + '<a class="red" id="autobattle">ボス探索</a>'
                   + '</div>');
    
        $('#autobattle').click(
            function () {
                $(this).text('実行中');
                eveBoss();
            }
        );
    }
};

if ($('div div').text().indexOf('セッション') >= 0) {
    // セッション切れ
    sessionLost();
}
else {
    // http://minerva.g-gee.info/index.php?view=event_questEvent&action=top&eventid=3&_t=1&opensocial_owner_id=100019732565&h=1393503511&guid=ON
    $('#head_menu')
        .height(400)
        .prepend(
        '<div style="text-align:center"><a href="'
        + makeurl(
            'http://minerva.g-gee.info/index.php',
            {
                view : 'event_questEvent',
                action : 'top',
                eventid : 3,
                _t : 1,
                opensocial_owner_id : 100019732565,
                h : Math.round(new Date().getTime() / 1000),
                guid : 'ON'
            })
        + '"><img src="http://minerva.g-gee.info/img_sp/event_q3/bn_mypage_140227.jpg" width="100%"></a></div>');

    if (view[params.view]) {
        view[params.view]();
    }
}

function quest() {
    var
    actNode = $($('td > span')[0]),
    textNode = actNode[0].nextSibling,
    newText = textNode.textContent
    ;
    textNode.parentNode.removeChild(textNode);
    actNode.after(newText.replace(/^([^\d]*)(\d+)(.*)$/, "$1<span id='act'>$2</span>$3"));
    $('body > div').before(
        "<input id='quest' type='button' value='クエスト！！'>"
    );
    $('#quest').click(doquest);
}

function doquest(questId) {
    $.get(
        makeurl(
            'http://minerva.g-gee.info/index.php',
            {
                view : 'flashQuestExecution',
                questid : questId,
                _t : 1,
                opensocial_owner_id : 100019732565,
                h : Math.round(new Date().getTime() / 1000),
                guid : 'ON'
            }),
        function () {
            getEnemyList(
                function(result) {
                    if (result.some(function (enemy) {return enemy.isNew;})) {
                        // 新規
                        location.reload();
                    }
                    else {
                        // いない
                        getStatus(
                            function(result) {
                                $('#act').text(result.actCost);
                                if (result.actCost >= 36) {
                                    doquest(questId);
                                }
                            });
                    }
                }
            );
        }
    );
}

function bossquest(questId, count) {
    var
    originalquestId = questId;
    if (count < 5) {
        questId = 4;
    }

    $.get(
        makeurl(
            'http://minerva.g-gee.info/index.php',
            {
                view : 'flashQuestExecution',
                questid : questId,
                _t : 1,
                opensocial_owner_id : 100019732565,
                h : Math.round(new Date().getTime() / 1000),
                guid : 'ON'
            }),
        function (data) {
            var
            match = data.match(/http:..minerva.g-gee.info.index.php[^'"]*/),
            flushurl = match && match[0]
            ;

            flushurl &&
                $.get(
                    flushurl,
                    function (data) {
                        var
                        match = data.match(/http:.*?minerva.g-gee.info[^'"]*/),
                        nexturl = match && match[0]
                        ;
                        if (nexturl && nexturl.match(/view=questDropCard/)) {
                            // カードゲット
                            getStatus(
                                function(result) {
                                    $('#act').text(result.actCost);
                                    bossquest(originalquestId, count + 1);
                                });
                        }
                        else {
                            // その他は遷移
                            location = nexturl;
                        }
                    }
                );
        }
    );
}

function encounterBoss() {
    var
    hpNode = $('img + div > span:nth-of-type(1)'),
    hpmatch = /(\d+)( \/ \d+)/.exec(hpNode[0].nextSibling.textContent),
    attackNode = $('body > div > img + div > div > span').filter(function (x){return /^攻撃コスト/.test($(this).text());}),
    attackMatch = / (\d+)( \/ \d+)/.exec(attackNode[0].nextSibling.textContent)
    ;
    hpNode[0].nextSibling.textContent = hpmatch[2];
    hpNode.after("<span id='hp'>");
    $('#hp').text(hpmatch[1]);
    attackNode[0].nextSibling.textContent = attackMatch[2];
    attackNode.after("<span id='attack'>");
    $('#attack').text(attackMatch[1]);
    // 強襲戦
    $('body > div').before(
        "<input id='battle' type='button' value='戦う！！'>"
        + "<div name='result'>"
    );
    $('#battle').click(battle);
}

function battle () {
    var
    rashNode = $('body > div > img + div > div > img + span');
    $.get(
        $('a.shortcut').attr('href'),
        function (data) {
            $.get(
                location.href,
                function (data) {
                    var hpMatch = /HP : <\/span>(\d+) \/ (\d+)/.exec(data);
                    var attackMatch = /攻撃コスト :<\/span> (\d+) \/ (\d+)/.exec(data);
                    var rashMatch = /残り\d+秒/.exec(data);
                    if (hpMatch && attackMatch) {
                        var
                        hp = hpMatch[1],
                        attack = attackMatch[1]
                        ;
                        $('#hp').text(hpMatch[1]);
                        $('#attack').text(attackMatch[1]);
                        if (rashNode.length > 0) {
                            if (rashMatch) {
                                rashNode.text(rashMatch[0]);
                            }
                            else {
                                location.reload();
                            }
                        }
                        if (attack > 24) {
                            battle();
                            return;
                        }
                    }
                    // リロード
                    location.reload();
                }
            );
        });
}


function sessionLost() {
    $('body > div').before(
        "<input id='cookie' type='text' value='"
        + cookies.MURAMASA + "'>"
        + "<input id='setcookie' type='button' value='OK'>"
    );
    $('#setcookie').click(
        function () {
            document.cookie = "MURAMASA=" + $('#cookie').val();
            location.reload();
        }
    );
}

function getStatus(callback) {
    var
    result = {},
    dfd = $.Deferred()
    ;

    $.get(
        'http://minerva.g-gee.info/index.php',
        {
            view : 'specialItem',
            itemid : 1,
            opensocial_owner_id : 100019732565,
            h : Math.round(new Date().getTime() / 1000),
            guid : 'ON'
        },
        function (data) {
            var
            actMatch = /行動力[^\d]+(\d+)\//.exec(data),
            attackMatch = /攻撃コスト[^\d]+(\d+)\//.exec(data)
            ;
            result.actCost = actMatch[1];
            result.attackCost = attackMatch[1];
            if (callback) {
                callback(result);
            }
            dfd.resolve(result);
        }
    );

    return dfd.promise();
}

function getEnemyList(callback) {
    $.get(
        'http://minerva.g-gee.info/index.php',
        {
            view : 'encounterBossList',
            page : null,
            _t : 1,
            opensocial_owner_id : 100019732565,
            h : Math.round(new Date().getTime() / 1000),
            guid : 'ON'
        },
        function (data) {
            var
            enemyReg = /#ffc000">([^<]+)<\/span>に<br>(<a href)?[\s\S]*?(\d+)人参戦[\s\S]*?encounterid=(\d+)[\s\S]*?(NEW)? <\/b>戦闘中[\s\S]*?HP :[^\d]*(\d+)[^\d]*(\d+)[\s\S]*?制限時間[^\d]*(\d\d:\d\d:\d\d)/mg,
            match,
            result = []
            ;

            while ((match = enemyReg.exec(data))) {
                result.push(
                    {
                        name : match[1],
                        isMine : !match[2],
                        ninzuu : match[3],
                        encounterid : match[4],
                        isNew : !match[2] ? match[3] == 0 : match[5],
                        hp : match[6],
                        maxhp : match[7],
                        limittime : match[8]
                    }
                );
            }
            
            callback(result);
        }
    );
}

function addevebattle () {
    $('tr')
        .each(
            function() {
                var params = getparams($(this).find('a').attr('href'));
                $(this).after(
                    '<tr>'
                        + '<td><a href="'
                        + makeurl(
                            'http://minerva.g-gee.info/index.php',
                            {
                                view : 'event_pvpBattle',
                                action : 'start',
                                encounterid : 0,
                                questid : 4,
                                init_view : 1,
                                eventno : 76,
                                eventid : 1,
                                deck_id : 0,
                                other_id : params.other_id,
                                encounter_flg : 1,
                                is_quest : 1,
                                _t : 1,
                                MURAMASA : cookies.MURAMASA,
                                nocache : 1
                            })
                        + '" class="mini_button darkred">クエバト</a></td>'
                        + '<td><a href="'
                        + makeurl(
                            'http://minerva.g-gee.info/index.php',
                            {
                                view : 'event_pvpBattle',
                                action : 'before',
                                eventid : 1,
                                other_id : params.other_id,
                                _t : 1,
                                opensocial_owner_id : 100019732565,
                                h : Math.round(new Date().getTime() / 1000),
                                guid : "ON"
                            })
                        + '" class="mini_button darkred">コロバト</a></td>'
                        + '</tr>');
            });
}

// パラメータハッシュ化
function getparams (url) {
    return url.replace(/.*\?/, "")
        .split('&')
        .reduce(
            function (hash, x) {
                var datas = x.split('=');
                if (hash[datas[0]]) {
                    if (!(hash[datas[0]] instanceof Array)) {
                        hash[datas[0]] = [hash[datas[0]]];
                    }
                    hash[datas[0]].push(datas[1]);
                }
                else {
                    hash[datas[0]] = datas[1];
                }
                return hash;
            },{});
}

function makeurl (url, params) {
    var
    result = url,
    paramlist = [],
    key
    ;
    if (params) {
        for (key in params) {
            paramlist.push(key + '=' + params[key]);
        }

        result += '?' + paramlist.join('&');
    }

    return result;
}

function questlist () {
    $('.button')
        .each(
            function () {
                var params = getparams($(this).find('form').attr('action'));
                $(this).append('<input type="button" value="自動実行" questid="'
                               + params.questid
                               + '" class="red autoquest">');
            });
    $('.autoquest').click(
        function () {
            doquest($(this).attr('questid'));
        });
}

function docallback(data, callbacklist) {
    var
    callback = callbacklist.shift();

    if (callback) {
        callback(
            data,
            function (data) {
                docallback(data, callbacklist);
            });
    }
}

function evetop() {
    $('#head_menu')
        .after('<div class="button">'
               + '<a class="red" id="autobattle">オートバトル</a>'
               + '</div>');
    
    $('#autobattle').click(
        function () {
            $(this).text('実行中');
            evebattle([]);
        }
    );
}

function evebattle (otherid_list) {
    var
    other_id;

    if (otherid_list.length < 2) {
        $.get(
            makeurl(
                'http://minerva.g-gee.info/index.php',
                {
                    view : 'event_pvpBattle',
                    action : 'search',
                    eventid : 1,
                    _t : 1,
                    opensocial_owner_id : 100019732565,
                    h : Math.round(new Date().getTime() / 1000),
                    guid : 'ON'
                }),
            function (result) {
                evebattle(
                    otherid_list.concat.apply(
                        otherid_list,
                        $(result)
                            .find('tr')
                            .filter(
                                function(){
                                    return $(this)
                                        .find('td + td > img')
                                        .attr('src')
                                        .match(/card\/[1-3]/);
                                })
                            .map(
                                function() {
                                    return getparams($(this)
                                                     .find('a')
                                                     .attr('href')).other_id;
                                })
                            .filter(
                                function (index, other_id) {
                                    return !blacklist[other_id];
                                }
                            )
                    ));
            }
        );
        return;
    }

    // ステータス更新
    $.get(
        makeurl(
            'http://minerva.g-gee.info/index.php',
            {
                view : 'event_pvpTop',
                action : 'top',
                eventid : 1,
                _t : 1,
                opensocial_owner_id : 100019732565,
                h : Math.round(new Date().getTime() / 1000),
                guid : 'ON'
            }),
        function (result) {
            var
            baseNode = $('tr'),
            resultNode = $(result).find('tr'),
            i;
            for (i = 0; i <= 2; i++) {
                baseNode[i].innerHTML = resultNode[i].innerHTML;
            }
            $('table + div')[0].innerHTML
                = $(result).find('table + div')[0].innerHTML;
        }
    );

    other_id = otherid_list.shift();
    console.log(other_id + 'とバトル開始');
    $.get(
        makeurl(
            'http://minerva.g-gee.info/index.php',
            {
                view : 'event_pvpBattle',
                action : 'start',
                other_id : other_id,
                deck_id : 3,
                eventid : 1,
                match_id : '',
                _t : 1,
                opensocial_owner_id : 100019732565,
                h : Math.round(new Date().getTime() / 1000),
                guid : 'ON'
            }),
        function (result) {
            var
            match = result.match(/createBlade.*?['"]([^"']*)['"]/)
            ;
            if (match) {
                getNextUrl(
                    match[1],
                    function (url) {
                        $.get(
                            url,
                            function (result) {
                                if (result.match(/win_bar/)) {
                                    console.log('勝ち！！');
                                    otherid_list.push(other_id);
                                }
                                else {
                                    console.log('負け');
                                    blacklist[other_id] = 1;
                                    localStorage.setItem(
                                        'blacklist', JSON.stringify(blacklist));
                                }
                                evebattle(otherid_list);
                            });
                    }
                );
                return;
            }
            else if (result.match(/ごめんなさい､うまく繋がらないみたい/)) {
                console.log('接続エラー再戦');
                otherid_list.unshift(other_id);
                evebattle(otherid_list);
                return;
            }
            else if (!result.match(/バトルできませんでした/)) {
                console.log('体力なし薬');
                useItem(
                    8,
                    function () {
                        otherid_list.unshift(other_id);
                        evebattle(otherid_list);
                    });
                return;
            }
            console.log('バトルできませんでした');
            evebattle(otherid_list);
        }
    );
}

function useItem(itemid, callback) {
    $.get(
        makeurl(
            'http://minerva.g-gee.info/index.php',
            {
                view : 'specialItem',
                _t : 1,
                opensocial_owner_id : 100019732565,
                h : Math.round(new Date().getTime() / 1000),
                guid : 'ON',
                action : 'useItem',
                itemid : itemid
            }),
        function (result) {
            if (callback) {
                callback();
            }
        }
    );
}

function getNextUrl(url, callback) {
    var
    dfd = $.Deferred()
    ;

    getData(
        url
    ).then(
        function (result) {
            var
            match = result.match(/[^'"]*index.php[^'"]*/),
            newurl = match && match[0]
            ;
            if (newurl) {
                if (newurl.match(/flash/)) {
                    getNextUrl(
                        newurl,callback
                    ).then(
                        function (url) {
                            dfd.resolve(url);
                        },
                        function (error, url) {
                            dfd.reject(error, url);
                        }
                    );
                }
                else {
                    if (callback) {
                        callback(newurl);
                    }

                    dfd.resolve(newurl);
                }
            }
            else {
                console.log(result);
                dfd.reject('no url', url);
            }
        },
        function (error) {
            dfd.reject(error, url);
        }
    );

    return dfd.promise();
}

function eveBoss(count) {
    var
    questId = 96,
    options
    ;

    if (!count) {
        count = 0;
    }
    if (count < 5) {
        questId = 4;
    }

// http://minerva.g-gee.info/index.php?view=event_flashQuest&eventid=3&questid=4&_t=1&opensocial_owner_id=100019732565&h=1393632261&guid=ON
// http://minerva.g-gee.info/index.php?view=event_flashEventQuest&eventid=3&questid=96&_t=1&opensocial_owner_id=100019732565&h=1393631011&guid=ON
    options = {
        view : 'event_flashEventQuest',
        eventid : 3,
        questid : questId,
        _t : 1,
        opensocial_owner_id : 100019732565,
        h : 1,
        guid : 'ON'
    };

                                   
    getNextUrl(
        geturl(
            options
        )
    ).then(
        function (nexturl) {
            var
            dfd = $.Deferred();
            ;

            if (nexturl && 
                (nexturl.match(/questDropCard/) ||
                 nexturl.match(/questDropCollection/))) {
                dfd.resolve();
            }
            else {
                dfd.reject('boss', nexturl);
            }

            return dfd.promise();
        }
    ).then(
        // カードゲット
        function () {
// http://minerva.g-gee.info/index.php?view=event_questQuest&action=questList&eventid=3&storyid=23&_t=1&opensocial_owner_id=100019732565&h=1393669696&guid=ON
            return getData(
                {
                    view : 'event_questQuest',
                    action : 'questList',
                    eventid : 3,
                    storyid : 24,
                    h : 1,
                    guid : 'ON'
                });
        },
        function (error, nexturl) {
            // その他は遷移
            location = nexturl;
        }
    ).done(
        function (result) {
            // 表示入れ替え
            $('#head_menu ~ img ~ *'
             ).remove();
            $('#head_menu ~ img'
             ).after(
                 $(result
                  ).find('#head_menu ~ img ~ *')
             );

            eveBoss(count + 1);
        }
    );
}

function geturl(options) {
    if (options.h) {
        options.h = Math.round(new Date().getTime() / 1000);
    }
    options.opensocial_owner_id = 100019732565;
    return makeurl(
        '/index.php',
        options
    );
}

function getData(arg) {
    var
    dfd = $.Deferred(),
    url = arg,
    options = arg
    ;
    if ((typeof url) !== "string") {
        if (options.h) {
            options.h = Math.round(new Date().getTime() / 1000);
        }
        options.opensocial_owner_id = 100019732565;

        url = geturl(options);
    }

    $.get(
        url
    ).then(
        function (data) {
            if (data.match(/セッション/)) {
                dfd.reject('セッション');
            }
            else if (data.match(/ごめんなさい､うまく繋がらないみたい/)) {
                // リトライ
                getData(
                    url
                ).then(
                    function (data) {
                        dfd.resolve(data, url);
                    },
                    function (error, url) {
                        dfd.reject(error,url);
                    }
                );
            }
            else {
                dfd.resolve(data, url);
            }
        },
        function (error) {
            dfd.reject(error, url);
        }
    );
    
    return dfd.promise();
}

// アッティラ
// http://minerva.g-gee.info/index.php?view=gacha&action=detail&type=boxStepGacha&viewType=1&_t=1&opensocial_owner_id=100019732565&h=1393747001&guid=ON

// ミニスタンプ
// http://minerva.g-gee.info/index.php?view=gacha&action=detail&type=stamp26Gacha&opensocial_owner_id=100019732565&h=1393747088&guid=ON

function samegacha(message) {
    if (message.gacha == params.type) {
        setTimeout(
            function () {
                location = $('a.gacha').attr('href');
            },0
        );
    }
}

// プレゼントリスト1000マニ
// <form action="http://minerva.g-gee.info/index.php?view=presentList&amp;receive=1&amp;_t=1&amp;opensocial_owner_id=100019732565&amp;h=1393759761&amp;guid=ON" method="post">
//                <input type="hidden" name="id" value="123427649">
//                <input type="hidden" name="amount" value="1000">
//                <input type="hidden" name="type" value="3">
//                                    <div class="button small" style="margin:-30px 0 0 160px;width:130px;"><input type="submit" value="受け取る" class="blue uketoru"></div>
//                            </form>

// 図鑑 ガチャ 神
// http://minerva.g-gee.info/index.php?view=pictureBook&action=cardList&dictionary_id=1&series_id=1&_t=1&opensocial_owner_id=100019732565&h=1393775101&guid=ON
